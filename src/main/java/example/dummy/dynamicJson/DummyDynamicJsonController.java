package example.dummy.dynamicJson;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class DummyDynamicJsonController {

    private static final String template = "%s";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/dummy-dynamic", method = RequestMethod.GET)
    public DummyDynamicJsonResponse dummyDynamicJsonFormatted(@RequestParam(value = "dummyParam", defaultValue = "0") String name) {
        return new DummyDynamicJsonResponse(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping(value = "/dummy-dynamic-post", method = RequestMethod.POST)
    public DummyDynamicJsonResponse dummyDynamicJsonFormattedPost(@RequestBody String payload, @RequestParam(value = "dummyParam", defaultValue = "0") String name) {
        return new DummyDynamicJsonResponse(counter.incrementAndGet(), String.format(template, payload));
    }
}