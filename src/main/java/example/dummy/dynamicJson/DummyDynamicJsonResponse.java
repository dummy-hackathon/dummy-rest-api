package example.dummy.dynamicJson;

public class DummyDynamicJsonResponse {

    private final long id;
    private final String content;

    public DummyDynamicJsonResponse(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}