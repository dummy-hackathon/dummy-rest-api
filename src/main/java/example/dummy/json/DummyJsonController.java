package example.dummy.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.parser.JSONParser;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.io.InputStreamReader;

@RestController
public class DummyJsonController {

    private String getJsonResponse() {
        InputStream inStream;
        InputStreamReader inStreamReader;
        JSONParser parser = new JSONParser();

        try {
            inStream = DummyJsonController.class.getResourceAsStream("/data/dummy-response.json");
            inStreamReader = new InputStreamReader(inStream);
            return parser.parse(inStreamReader).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getFormattedJsonResponse() {
        Object formattedJsonResponse;
        String jsonResponse;
        ObjectMapper mapper = new ObjectMapper();

        try {
            jsonResponse = getJsonResponse();
            formattedJsonResponse = mapper.readValue(jsonResponse, Object.class);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(formattedJsonResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "/dummy-static", method = RequestMethod.GET)
    public String dummyStaticJsonFormatted() {
        return getFormattedJsonResponse();
    }

    @RequestMapping(value = "/dummy-static-post", method = RequestMethod.POST)
    public String dummyStaticJsonFormattedPost(@RequestBody String payload) {
        return payload;
    }
}